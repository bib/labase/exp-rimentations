* 1er test de nébuliseur Low-Tech au 15 avril
** Bricolage d'un système
Avec le tube d'un stylo vide, diamètre de 5mm + un capuchon de stylo, diamètre 3mm + impacteur fixé en bricolage à 2 mm de distance et d'un diamètre de 2-3mm.
Accroche du bouchon au tube grâce à un élastique pour faire en sorte d'avoir un espacement entre ces 2 éléments d'environ 1mm tout autour.

** Protocole du 1er test
Prendre un récipient qui puisse faire couler régulièrement de l'eau en faible quantité dans le système (dans l'espace entre le bouchon et le tube du stylo). Puis soufller de manière la constante et continue possible pour simuler la pression de l'air.

  - Visiblement *cette assemblage ne fonctionne pas, c'est un échec*. En effet, il y a une nébulisation grossière qui représente environ 1/5 du volume d'eau versé dans le système. Le reste de l'eau remonte et sort par le haut du bouchon.

** Plusieurs raisons possibles :
  - pression de l'air (avec la bouche) non constante.
  - Trop grande quantité d'eau versé d'un coup.
  - Le système doit être fermé ? Essayer avec un ballon de baudruche autour ?
  - Espacements et diamètres du système à revoir.
  
** Objectifs :
  - Multiplier les tests avec différentes variantes.
  - Ne pas le mettre complétement la tête en bas, plutôt en biais.
  - Mettre le système dans un contenant (stabilité, biais et espace fermé).
