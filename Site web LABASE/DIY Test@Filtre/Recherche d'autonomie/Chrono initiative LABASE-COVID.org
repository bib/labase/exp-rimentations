* Chronologie de l'activité de LABASE durant les événements covidiens

/Personnes actives :/ 
Géronimo, Jeanne, Lucas, Erwan, Pierre, Maya, Françoise, Lucile, Gilles, Jennifer, Gauthier, Tangui

** Période de confinement (17 mars au 11 mai)

Le collectif LABASE se réunit en quotidienne d'une durée d'environ 1h à 3h (à 14h sur la [[https://meet.jit.si/BIB-LABASE-COVID19][plateforme Jitsi]]). Au bout de 2 semaines nous avons décidé naturellement de mettre en pause la quotidienne le week-end. L'idée au démarrage est de mettre en place un espace d'entraide, de compréhension du COVID-19 et d'actions collectives ; De se poser collectivement les questions : comment réagir ? Que faire ? Comment être utile ?

/Remarque introspective :/ 
Les échanges d'informations, la collaboration sur des actions collectives ont rythmé nos journées confinées.
 
*** 23 mars 
 - Premier jour de la quotidienne : on décide de se retrouver tous les jours à 14H 
 - debut du dokuwiki pour la documentation -> outils utilisés  : gitlab (developpeur), wiki (libriste), zotero (chercheur), radio libre (cracker?) 
*** 25 mars
 - 1ère évocation d'un test de l'efficacité de filtration des masques. 
 - Géro documente les 3 premiers jours sur le dokuwiki puis créer un accès à Tangui, qui prend le relais. 
*** 26 mars
 - Recherche d'articles et d'informations sur l'épidémie et le virus du COVID-19 
 - Réflexion sur nos modes d'actions (réalisation de masques DIY + fabrication de gel hydro-alcoolique + ce que peut faire un biohackerspace) ainsi que recherche d'acteurs, collectifs, groupes actifs sur Montpellier pour s'auto-organiser en réseau d'entraide local. 
 - Début de l'initiative avec CoVie Entraide (se greffer à un collectif déjà dynamique pour impulser la dynamique de LABASE, mobiliser un maximum d'acteurs) pour fédérer, organiser, produire des outils collectifs et agir auprès des plus vulnérables (démarche solidaire)
*** 27 mars
 - Masque DIY créé par Tangui 
 - LEPIED (association de paysan-cuisinier) peut donner des legumes 
 - Réalisation d'un kit aidé et kit aidant pour réseau d'entraide local.
*** 28 mars
 - Partage d'infos fiables sur le COVID-19 et discussions sur la situation, le contexte actuel.
 - Nous implémentons une [[https://lebib.org/wiki/doku.php?id=labase:medecine-autonome:covid19][page sur le COVID-19]] pour rassembler les références, les sources, les inspirations, les initiatives et les informations covidiennes.
*** 30 mars
 - 2ème évocation du test 
 - Mise en place du [[https://www.zotero.org/groups/2462558/bib/library][Zotero]] 
 - Début du détachement progressif de CoVie Entraide après avoir structuré et mis en route les initiatives avec le collectif pour se focaliser sur les actions possibles du biohackerspace.
*** 2 avril
 - Notre choix s'est arrêté sur le projet (atelier) *DIY Test@Filtre* /expérimentations, protocoles, prototype, tests, documentation, résultats scientifiques, transmission et publication/. Notamment déclenché suite à une invitation à publier pour la [[https://lavoiedujaguar.net/][Voix du jaguar]]. Notons la présence de Lucas, Géronimo, Tangui et Jeanne, qui sont des chercheurs. La perspective de la publication et de trouver un moyen de répondre à des besoins de sociétés à motiver la concentration sur un projet propre à LABASE 
 - Nous parlons de [[https://www.afnor.org/actualites/coronavirus-telechargez-le-modele-de-masque-barriere/][Afnor]] pour la première fois 
 - Début des questionnements sur la conception, l'ingénierie de projet.
*** 3 avril 
 - Nous entrons dans une partie technique (effet venturi, nebulisation, boîte à toux, aérosol, bactériophage, analyses microbiologiques, bactéries pour simuler le virus).
 - Le processus des chaînes de coopération est lancé (process de création-production-recherche collective)
*** 6 avril
- Nous commençons à réaliser des schémas au travers de la visio pour expliquer nos idées, inventions, bricolages. Puis réalisons fréquement un schéma récapitulatif (évolutif) de notre dispositif DIY schéma. 
- Le [[https://framagit.org/bib/labase/exp-rimentations/-/tree/master/COVID19/MASKS%20Testings]][FramaGit]] démarre, notamment pour effectuer une documentation plus complète.
*** 8 avril 
 - Nous commençons à prototyper le dispositif DIY Test@Filtre que nous appelons alors "bouttière" puisque nous utilisons des matériaux de récupération, de recyclage ou très accessibles parce que communs pour le fabriquer (bouteille en plastique, éléments d'une cafetière italienne) -> [[https://framagit.org/bib/labase/exp-rimentations/-/tree/master/COVID19/MASKS%20Testings/Sch%C3%A9ma%20Syst%C3%A8me%20de%20Test%20Masque%20DIY][Schéma]].
*** 10 avril
 - Nous explorons la possibilité d'utiliser autre chose que des bactéries afin que ce soit plus accessible et reproductible. L'Atelier Métissé (atelier textile collaboratif à Avignon) a mis en place 3 tests empiriques pour connaître l'efficacité filtrante de leurs masques (durant la crise, ils ont produits jusqu'à 10 000 masques par semaine pour les collectivités, entreprises en associant une machine de découpe semi-industrielle et un réseau de couturier), notamment grâce à la granulométrie. Les masques avaient 3 couches et étaient fait en textile végétal
 - Tests empiriques : 
 1. Bougie s'eteind (DENSITÉ)
 2. Fer à repasser, pigment coloré dans la vapeur d'eau qui se retrouve de l'autre coté du masque (AEROSOLÉ)
 3. Spray de 3,5 bar (simulation toux), pigment qui traverse (PRESSION) 
*** Courant avril
 - Ingénierie de projet à distance : matériaux, choix, structure du dispositif, low-tech, DIY, écriture des protocoles, prototypages et expérimentations, visualisations. /(Annexe PDF compte-rendu Tangui : images, traces, captures écran, vidéos)/. [https://lebib.org/wiki/doku.php?id=labase:medecine-autonome:biblio-masks][Réalisation d'une bibliographie relative au Sars-cov-2]].
*** 1er mai
 - Lancement d'une [[https://www.helloasso.com/associations/le-bib-hackerspace/collectes/diy-test-filtre][page de financement participatif]] (crowdfunding) pour l'atelier DIY Test@Filtre.
*** 4 mai
 - Récupération d'une culture de bactérie par Géronimo : /Escherichia coli/

** période de déconfinement  (11 mai au 30 octobre)
Le confinement étant fini, la quotidienne COVID-19 de LABASE se retrouve en chair et en os (masqué) pour commencer les manipulations dans le laboratoire (BIB - La Tendresse).
*** 11 et 12 mai
 - Mise en place de l'atelier DIY Test@Filtre -> montage du premier prototype/dispositif de test : [[https://framagit.org/bib/labase/exp-rimentations/-/tree/master/COVID19/MASKS%20Testings/DIY%20TEST%40FILTRE_Atelier%20COVID-19/05-11et12][photos] 
 - Début de la réalisation du protocole de test -> nous avons conçu le protocole et dans ce même temps les novices/profanes en la matière ont pu se familiariser avec les manipulations en microbiologie.
*** 13 mai
 - Nous continuons les recherches sur les masques DIY et nous bricolons un dispositif de test - Test@Filtre - simplifié.
*** 14 mai
 - Mise en place d'un protocole plus clair et plus efficace. Le dispositif de test prêt, fonctionnel.
*** 18 mai
 - Réalisation d'un système pour capter la pression avec un arduino (Erwan surtout) 
 - schéma définitif
*** du 19 mai au 29 juillet
 - Réalisation des tests en fonction des différents textiles, évolution du protocole, collecte de données (comptage, cahier de manips, framacalc) et documentation (photos, notes, dokuwiki, framagit). Les textiles testés :
   1. Essui-tout (5marques)
   2. Lingettes dépoussiérantes (3 marques)
   3. Coton (t-shirt et chiffon)
   4. Wax
   5. Masques en tissu à 1,2 et 3 couches (collectivités, institutions, entreprises)
   6. Sac aspirateurs (4 marques)
   7. Blouses chirurgicales
   8. Masques chirurgicaux
   9. Assemblages
 - Documentation des données récoltées dans un carnet de manipulations dédié au labo au BIB et report sur un [[https://lite.framacalc.org/9hiz-testfiltre_resultats][calc-partagé]].
 - Rassemblement des élements de recherche et résultats sur le cloud du BIB.
 - Nous invitons chacun à venir au BIB, puisque LABASE peut maintenant vous proposer de tester vos masques ou n'importe quelle composition textile.
** Courant Août
 - C'est calme, on avance doucement, certains sont en vacances.
** Courant Septembre
 - Réalisation colective de 2 vidéos tutoriels et explicatives :
   1. [[https://www.youtube.com/watch?v=km8is-onO1w&feature=youtu.be&ab_channel=TanguiD][La première expliquant le montage du DIY Test@Filtre]]
   2. [[https://www.youtube.com/watch?v=9T8CL6yV5iU&feature=youtu.be&ab_channel=TanguiD][La seconde faisant la démonstration des différentes manipulations du protocole de test]]
 - Travail réalisé à LABASE, au labo, à distance grâce à Jitsi, ainsi qu'au cloud du BIB (espace de stockage et partage) :
   1. Traitement et analyses des données récoltées lors des tests.
   2. Représentation au travers de diagrammes.
   3. Approfondissement et vérifications des données, réalisation de nouveaux tests pour déterminer les conditions de test et des protocoles. Par exemple le débit, la pression, la respirabilité.
   4. Début de la finalisation de l'écriture de l'article côté microbio/virologie. Première ébauche des idées pour la partie socio-technique de l'article.
** Début octobre
 - Finalisation de la partie microbio et virologie de l'article.
 - Mise en marche de la rédaction de la partie socio-technique, notamment par la comparaison de l'initiative de la quotidienne COVID de LABASE et du DIY Test@Filtre avec les autres initiatives prises (hackaton, fab-lab, makeurs, etc) durant la crise. Puis par l'analyse des notes effectuées durant l'ensemble de l'initiative collective, afin de tenter de définir une grille d'analyse à propos de la recherche d'autonomie.
 - Organisation d'un atelier Test@Filtre dans le cadre de l'université d'automne du BIB (28 octobre – annulées pour cause de deuxième confinement)

** Période de reconfinement (30 octobre au ...)
*** mi- et fin-octobre (reconfinement)
 - Relance des tests : pour les données manquantes ainsi que pour préciser les données et résultats 
 - Avancée de l'article (microbio et socio-technique) dans l'objectif d'une publication et d'un vaste partage aux différentes communautés, collectifs, amis, compas.


/Comment ça s'est passé, les notes de documentation :/
https://lebib.org/wiki/doku.php?id=labase:medecine-autonome:notes-covid
et https://lebib.org/wiki/doku.php?id=labase:medecine-autonome:covid19